class Node {
    constructor(value){
        this.left = null;
        this.right = null;
        this.value = value;
    }
}

class BinaryTree {

    constructor(){
        this.root = null;
    }

    insert(value)  {
        const newNode = new Node(value);
        if( this.root === null ) this.root = newNode;
        else {
            let currentNode = this.root;
            while( true ) {
                if( value < currentNode.value ) {
                    if( !currentNode.left ) {
                        currentNode.left = newNode;
                        return this;
                    }
                    currentNode = currentNode.left;
                }
                else {
                    if( !currentNode.right ) {
                        currentNode.right = newNode;
                        return this;
                    }
                    currentNode = currentNode.right;
                }
            }
        }
    }

    lookup(value) {
        if( !this.root ) return undefined;
        let currentNode = this.root;
        while( currentNode ) {
            if( value < currentNode.value ) {
                currentNode = currentNode.left;
            } else if( value > currentNode.value ) {
                currentNode = currentNode.right;
            } else if( currentNode.value === value ) {
                return currentNode;
            }
        }
        return undefined;
    }

    showTree(node) {
        const tree = { value: node.value };
        tree.left = node.left === null ? null : this.showTree(node.left);
        tree.right = node.right === null ? null : this.showTree(node.right);
        return tree;
    }

    inOrder(root){ // LEFT - ROOT - RIGHT
        if( root ) {
            this.inOrder(root.left)
            console.log(root.value);
            this.inOrder(root.right)
        }     
    }

    postOrder(root){ // LEFT - RIGHT - ROOT
        if( root ) {
            this.postOrder(root.left)
            this.postOrder(root.right)
            console.log(root.value);
        }
    }

    preOrder(root){ // ROOT - LEFT - RIGHT
        if( root ) {
            console.log(root.value);
            this.preOrder(root.left)
            this.preOrder(root.right)
        }
    }

    deleteNode(value){
        if( !this.root ) return null;
        let currentNode = this.root;
        let fatherNode = null;

        while( currentNode ){
            // Cuanto el valor es menor al valor del nodo actual, se busca hacia la izquierda
            if( value < currentNode.value ){ 
                fatherNode = currentNode;
                currentNode = currentNode.left;
            } 
            // Cuanto el valor es mayor al valor del nodo actual, se busca hacia la derecha
            else if( value > currentNode.value ){
                fatherNode = currentNode;
                currentNode = currentNode.right;
            } 
            // Cuando el valor es igual al valor del nodo actual existen 3 casos
            else if ( value === currentNode.value ) {
                // caso 1: el nodo no tiene hijos
                if ( !currentNode.right && !currentNode.left){
                    if( fatherNode.right === currentNode )
                        fatherNode.right = null;
                    else
                        fatherNode.left = null;
                    return this;
                }
                // caso 2: el nodo tiene 1 hijo
                else if ( !currentNode.right || !currentNode.left ){
                    if( fatherNode.right === currentNode )
                        fatherNode.right = currentNode.right ? currentNode.right : currentNode.left;
                    else 
                        fatherNode.left = currentNode.right ? currentNode.right : currentNode.left;
                    return this;
                }
                // caso 3: el nodo tiene 2 hijos
                else {
                    let fatherOfMinNode = currentNode.right;
                    let minNode = currentNode.right.left;
                    while (minNode && minNode.left) {
                        fatherOfMinNode = minNode;
                        minNode = minNode.left;
                    }
                    if( !minNode ){
                        minNode = fatherOfMinNode;
                        minNode.right = null;
                        minNode.left = currentNode.left;
                        if( fatherNode.right === currentNode )
                            fatherNode.right = minNode;
                        else 
                            fatherNode.left = minNode;
                    } else {
                        minNode.right = currentNode.right;
                        minNode.left = currentNode.left;
                        fatherNode.right = minNode;
                        fatherOfMinNode.left = null;
                    }
                    return this;
                }

            }
            else {
                return null;
            }
        }
    }

    deleteNodeRecursive(root,value) { 
        if( !root ) return null;
        if( value < root.value ){
            root.left = this.deleteNodeRecursive(root.left, value);
        } else if( value > root.value ) {
            root.right = this.deleteNodeRecursive(root.right, value);
        } else {
            if( root.left === null){
                const temp = root.right;
                return temp;
            } else if( root.right === null ){
                const temp = root.left;
                return temp;
            }
            const temp = this.getMin(root.right);
            root.value = temp.value;
            root.right = this.deleteNodeRecursive(root.right, temp.value);
        }
        return root;
    }

    getMin(node) {
        let currentNode = node;
        while (currentNode && currentNode.left) {
            currentNode = currentNode.left;
        }
        return currentNode;
    }

}

const tree = new BinaryTree();
tree.insert(4);
tree.insert(2);
tree.insert(1);
tree.insert(3);
tree.insert(6);
tree.insert(5);
tree.insert(7);
//console.log(tree.lookup(2));
//console.log(tree.deleteNodeRecursive(tree.root,1));
//console.log(tree.deleteNode(21));
tree.preOrder(tree.root);
//console.log(JSON.stringify(tree.showTree(tree.root)));

  