class Node {
    constructor(value){
        this.value  = value;
        this.next   = null;
    }
}

class LinkedList {
    constructor(value){
        this.head = new Node(value);
        this.tail = this.head;
        this.lenght = 1;
    }

    append(value){
        const newNode = new Node(value);
        this.tail.next = newNode;
        this.tail = newNode;
        this.lenght++;
    }

    prepend(value){
        const newNode = new Node(value);
        newNode.next = this.head;
        this.head = newNode;
        this.lenght++;
    }

    printList(){
        const array = [];
        let currentNode = this.head;
        while( currentNode !== null ) {
            array.push(currentNode.value);
            currentNode = currentNode.next;
        }
        return array;
    }

    insert(index, value) {
        if( index >= this.lenght ){
            return this.append(value);
        }
        let newNode = new Node(value);
        const previusNode = this.getPreviousNodeByIndex(index-1);
        const nextNode = previusNode.next;
        
        previusNode.next = newNode;
        newNode.next = nextNode
        this.lenght++;
    }

    getPreviousNodeByIndex(index){
        let counter = 0;
        let currentNode = this.head;
        while( counter !== index ){
            currentNode = currentNode.next;
            counter++;
        }
        return currentNode;
    }

    remove(index){
        const previusNode = this.getPreviousNodeByIndex(index-1);
        const unwantednode = previusNode.next;
        previusNode.next = unwantednode.next;
        this.lenght--;
    }

}

const linkedList = new LinkedList(10);
linkedList.append(15);
linkedList.append(25);
linkedList.prepend(5);
linkedList.insert(2,69);
linkedList.insert(20,44);
linkedList.remove(3);
console.log(linkedList.printList());
