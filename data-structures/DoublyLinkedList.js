class Node {
    constructor(value){
        this.value      = value;
        this.next       = null,
        this.previus    = null;
    }
}

class DoublyLinkedList {
    constructor(value){
        this.head = new Node(value);
        this.tail = this.head;
        this.lenght = 1;
    }

    append(value){
        const newNode = new Node(value);
        newNode.previus = this.tail;
        this.tail.next = newNode;
        this.tail = newNode;
        this.lenght++;
    }

    prepend(value){
        const newNode = new Node(value);
        newNode.next = this.head;
        this.head.previus = newNode;
        this.head = newNode;
        this.lenght++;
    }

    printList(){
        const array = [];
        let currentNode = this.head;
        while( currentNode !== null ) {
            array.push(currentNode.value);
            currentNode = currentNode.next;
        }
        return array;
    }

    insert(index, value) {
        if( index >= this.lenght ){
            return this.append(value);
        }
        let newNode = new Node(value);
        const previusNode = this.getPreviousNodeByIndex(index-1);
        const nextNode = previusNode.next;
        
        previusNode.next = newNode;
        newNode.previus = previusNode;
        newNode.next = nextNode
        nextNode.previus = newNode;
        this.lenght++;
    }

    getPreviousNodeByIndex(index){
        let counter = 0;
        let currentNode = this.head;
        while( counter !== index ){
            currentNode = currentNode.next;
            counter++;
        }
        return currentNode;
    }

    remove(index){
        const previusNode = this.getPreviousNodeByIndex(index-1);
        const unwantednode = previusNode.next;
        const nextNode = unwantednode.next;

        previusNode.next = unwantednode.next;
        nextNode.previus = previusNode;
        
        this.lenght--;
    }

}

const linkedList = new DoublyLinkedList(10);
linkedList.append(15);
linkedList.append(25);
linkedList.prepend(5);
linkedList.insert(2,69);
linkedList.insert(20,44);
linkedList.remove(3);
console.log(linkedList.printList());