class Stack {

    constructor(){
      this.array = new Array();
    }

    peek() {
        return this.array[this.array.length-1];
    }

    push(value) {
        this.array.push(value);
    }

    pop() {
        this.array.pop();
    }

}
  
  const myStack = new Stack();
  myStack.push('google');
  myStack.push('udemy');
  myStack.push('discord');
  myStack.pop();
  console.log(myStack.peek());
  console.log(myStack);