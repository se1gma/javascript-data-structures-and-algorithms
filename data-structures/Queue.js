class Queue {

    constructor(){
        this.array = new Array();
    }

    peek() {
        return this.array[this.array.length-1];
    }

    enqueue(value) {
        this.array.push(value);
    }

    dequeue() {
        this.array.shift();
    }
}

const myQueue = new Queue();
myQueue.enqueue('google');
myQueue.enqueue('udemy');
myQueue.enqueue('discord');
console.log(myQueue.peek());
console.log(myQueue);
myQueue.dequeue();
console.log(myQueue);
