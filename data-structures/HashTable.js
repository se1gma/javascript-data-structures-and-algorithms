class Hashtable {
    data;

    constructor(size) {
        this.data = new Array(size);
    }

    _hash(key){
        let hash = 0;
        for( let i = 0; i<key.length; i++ ){
            hash += key.charCodeAt(i);
        }
        return hash % this.data.length;
    }

    set(key,value){
        let address = this._hash(key);
        if ( !this.data[address] ) { // Si no encuentra nada lo agrega
            this.data[address] = [];
            this.data[address].push([key,value]);
        } else { // si encuentra algo, debe recorrer el arreglo encontrar la key y remplazar el valor
            for ( let i = 0; i < this.data[address].length; i++ ) {
                if ( this.data[address][i][0] === key) {
                  this.data[address][i][1] = value;
                  return;
                }
            } // si no encunetra la key lo agrega al final
            this.data[address].push([key, value]);
        }
    }

    get(key){
        let address = this._hash(key);
        const currentBucket = this.data[address];
        if( currentBucket ){
            for( let i = 0; i<currentBucket.length; i++ ){
                if ( currentBucket[i][0] === key )
                    return currentBucket[i][1];
            }          
        }
        return undefined;
    }

    display() {
        for( let i=0; i < this.data.length; i++ ){
            console.log(this.data[i]);
        }
    }

}

let hashtable = new Hashtable(2);
hashtable.set('apples',100);
hashtable.set('apples',150);
hashtable.set('grapes',50);
hashtable.set('watermelon',10);
hashtable.display();