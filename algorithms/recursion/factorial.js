const findFactorialNumberIterative = (number) => {
    let answer = 1;
    if( number == 2) answer =2;
    for( let i=2; i<=number; i++){
        answer = answer * i;
    }
    return answer;
};

const findFactorialNumberFactorial = (number) => {
    if( number == 2) return 2;
    return number*findFactorialNumberFactorial(number-1);
};
console.log(findFactorialNumberIterative(5));
console.log(findFactorialNumberFactorial(5));