const fibonacciIterative = (number) => { // O(n) = n
    let previusNumber = 0;
    let nextNumber = 1;
    for( let i=1; i<number; i++ ) {
        answer = previusNumber + nextNumber;
        previusNumber = nextNumber;
        nextNumber = answer;
    }
    return answer;
};

const fibonacciIterative2 = (number) => { // O(n) = n
    let array = [0,1];
    for( let i=2; i<=number; i++ ) {
       array.push(array[i-1]+array[i-2]);
    }
    return array[number];
};

const fibonacciFactorial = (number) => { // O(n) = 2^n
   if( number < 2 ) return number;
   return fibonacciFactorial(number-1) + fibonacciFactorial(number-2);
};

console.log(fibonacciIterative(5));
console.log(fibonacciIterative2(5));
console.log(fibonacciFactorial(5));