const reverseRecursive = (str) => {
    if( str.length === 1 ) return str[0];
    let revertedString = str[str.length-1];
    revertedString = revertedString + reverseRecursive(str.slice(0,str.length-1));
    return revertedString;
};

console.log(reverseRecursive('hola como'));