const numbers = [0, 1, 2, 4, 5, 6, 44, 63, 87, 99, 283];
const itemToSearch = 55;

const binarySearch = (array, itemToSearch) => {
    let start = 0;
    let end = array.length-1;
    while( start <= end ){
        let middle = Math.floor((start+end)/2);
        if( array[middle] === itemToSearch ) return true;
        else if( array[middle] < itemToSearch ) start = middle+1;
        else  end = middle-1;
    }
    return false;
};

const binarySearchRecursive = (array, start, end, itemToSearch) => {
    
    if( start > end ) return false;
    
    let middle = Math.floor((start+end)/2);

    if( array[middle] === itemToSearch ) return true;

    if( array[middle] < itemToSearch ) return binarySearchRecursive(array, middle+1, end, itemToSearch);
    else return binarySearchRecursive(array,start, middle-1, itemToSearch);
};

const answer = binarySearch(numbers, itemToSearch);
const answer2 = binarySearchRecursive(numbers, 0, numbers.length-1, itemToSearch);

console.log(answer);
console.log(answer2);