const numbers = [99, 44, 6, 2, 1, 5, 63, 87, 283, 4, 0];

let quickSort = (array) => {
    if (array.length < 2) return array; // No se iguala a 1 porque un lado de los arreglos puede venir vacio
    const pivot = array[Math.floor(Math.random() * array.length)];

    let left = [];
    let right = [];
    let equal = [];

    for (let val of array) {
        if (val < pivot) {
        left.push(val);
        } else if (val > pivot) {
        right.push(val);
        } else {
        equal.push(val);
        }
    }
    return [ ...quickSort(left), ...equal, ...quickSort(right) ];
}

const answer = quickSort(numbers);
console.log(answer);