const numbers = [99,44,6,2,1,5,63,87,283,4,0];

let selectionSort = (array) => {
    for(let i=0; i < array.length; i++){
        let indexMin = i;
        for(let j=i+1; j < array.length; j++){
            if( array[j] < array[indexMin] ) {
                indexMin = j;
            }
        }
        let temp = array[i];
        array[i] = array[indexMin];
        array[indexMin] = temp;
    }
    return array;
};


console.log(selectionSort(numbers));