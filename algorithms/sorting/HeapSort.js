/*
** Si bien este algoritmo se representa visualmente como un arbol binario, 
** pero en realidad su implementacion se basa netamente en un array,
** por lo tanto no se necesitan conocimientos de arboles para implementarlo
**
** Tamaño arreglo = n
** Ultimo padre (indice), i = n/2
** Left child = 2*i + 1 
** Right child = 2*i + 2
*/
const numbers = [99, 44, 6, 2, 1, 5, 63, 87, 283, 4, 0];

const swap = (array, firstElement, secondElement) => {
    const temp = array[firstElement];
    array[firstElement] = array[secondElement];
    array[secondElement] = temp;
};

const heapify = (array, length, i) => {
    let largest = i;
    let leftChild = 2*i + 1;
    let rightChild = 2*i + 2;

    if( array[leftChild] >  array[largest] && leftChild < length ) {
        largest = leftChild;
    }

    if( array[rightChild] >  array[largest] && rightChild < length ) {
        largest = rightChild;
    }

    if( largest != i ){
        swap( array, i, largest );
        heapify( array, length, largest) ;
    }
};

const buildMaxHeap = (array, length, arrayLength) => {
    for( let i=length-1; i>=0; i--) {
        heapify( array, arrayLength, i ); // Primer recorrido para llevar el mayor valor a la cima
    }
};
  
const heapSort = (array) => {
    let arrayLength = array.length;
    let lastParent = Math.floor(arrayLength/2);
    buildMaxHeap(array, lastParent, arrayLength);

    for( let i=arrayLength-1; i>=0; i--) {
        swap( array, 0, i );
        heapify( array, i, 0 );
    }
    return array;
};

const answer = heapSort(numbers);
console.log(answer);