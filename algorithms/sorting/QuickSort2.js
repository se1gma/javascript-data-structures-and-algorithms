const numbers = [99, 44, 6, 2, 1, 5, 63, 87, 283, 4, 0];

let quickSort = (array, left, right) => {
    if (array.length === 1) return array;

    const index = partition( array, left, right );
    
    if( left < index-1 ) quickSort( array, left, index-1 );
    if( right > index ) quickSort( array, index, right );
    
    return array;
};

let partition = (array, left, right) => {
    const middle    = Math.floor( (left+right)/2 );
    const pivot     = array[middle];
  
    while ( left <= right ) {
        while( array[left] < pivot )
            left++;
        while( array[right] > pivot )
            right--;
        if( left<=right ){
            swap(array, left, right);
            left++;
            right--;
        }
    }
    return left; // Devuelve el valor de indice del pivote +1
};

let swap = ( array, leftIndex, rightIndex ) => {
    let temp = array[leftIndex];
    array[leftIndex] = array[rightIndex];
    array[rightIndex] = temp;
};
  
  
const answer = quickSort(numbers, 0, numbers.length - 1);
console.log(answer);