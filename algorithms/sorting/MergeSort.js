const numbers = [99, 44, 6, 2, 1, 5, 63, 87, 283, 4, 0];

let mergeSort = (array) => {
  if (array.length === 1) return array

  const length  = array.length;
  const middle  = Math.floor(length/2);
  const left    = array.slice(0,middle);
  const right   = array.slice(middle,length);

  return merge( mergeSort(left), mergeSort(right) );
};

let merge = (left, right) => {
  const result = [];
  while( left.length && right.length ) {
    if( left[0] < right[0] ) 
        result.push(left.shift());
    else
        result.push(right.shift());
  }
  return [...result, ...left, ...right];
};

const answer = mergeSort(numbers);
console.log(answer);