const numbers = [99,44,6,2,1,5,63,87,283,4,0];

let insertionSort = (array) => {
  for ( j = 1; j < array.length; j++ ) {
    let key = array[j];
    let i	= j - 1;
    while( i >= 0 && array[i] > key ) {
      array[i+1] = array[i];
      i = i - 1;
    }
    array[i+1] = key;
  }
  return array;
};

console.log(insertionSort(numbers));
