const maxSubArray = datos => {
  let max = datos[0];
	let suma = 0; 
	for ( let j=0; j<datos.length; j++ ) {
    suma = suma + datos[j];
    if ( datos[j] > suma ) suma = datos[j];
		if ( suma > max ) max = suma;
	}
  return max;
};

maxSubArray([-2,1,-3,4,-1,2,1,-5,4]);