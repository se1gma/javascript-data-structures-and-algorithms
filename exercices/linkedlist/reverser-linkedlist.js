class Node {
    constructor(value){
        this.value  = value;
        this.next   = null;
    }
}

class LinkedList {
    constructor(value){
        this.head = new Node(value);
        this.tail = this.head;
        this.lenght = 1;
    }

    append(value){
        const newNode = new Node(value);
        this.tail.next = newNode;
        this.tail = newNode;
        this.lenght++;
    }

    prepend(value){
        const newNode = new Node(value);
        newNode.next = this.head;
        this.head = newNode;
        this.lenght++;
    }

    printList(){
        const array = [];
        let currentNode = this.head;
        while( currentNode !== null ) {
            array.push(currentNode.value);
            currentNode = currentNode.next;
        }
        return array;
    }

    insert(index, value) {
        if( index >= this.lenght ){
            return this.append(value);
        }
        let newNode = new Node(value);
        const previusNode = this.getPreviousNodeByIndex(index-1);
        const nextNode = previusNode.next;
        
        previusNode.next = newNode;
        newNode.next = nextNode
        this.lenght++;
    }

    getPreviousNodeByIndex(index){
        let counter = 0;
        let currentNode = this.head;
        while( counter !== index ){
            currentNode = currentNode.next;
            counter++;
        }
        return currentNode;
    }

    remove(index){
        const previusNode = this.getPreviousNodeByIndex(index-1);
        const unwantednode = previusNode.next;
        previusNode.next = unwantednode.next;
        this.lenght--;
    }

    reverse(){
        if( !this.head.next ) return this.head;
        let first = this.head;
        this.tail = this.head;
        let second = this.head.next;
        while(second){
            const temp = second.next;
            second.next = first;
            first = second;
            second = temp;
        }
        this.head.next = null;
        this.head = first;
    }

    reverse2(){
        if( !this.head.next ) return this.head;
        let current = this.head;
        this.tail = current;
        let prev = null;
        while (current) {
            let temp = current.next;
            current.next = prev;
            prev = current;
            current = temp;
        }
        this.head = prev;
    }

}

const linkedList = new LinkedList(10);
linkedList.append(15);
linkedList.append(25);
linkedList.reverse2();
console.log(linkedList.printList());
console.log(linkedList);
