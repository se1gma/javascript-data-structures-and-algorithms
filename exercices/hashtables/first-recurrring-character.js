function firstRecurrringCharacter(input){
    for(let i=1; i<input.length;i++){
        for(let j=i-1;j>=0;j--){
            if ( input[i] === input[j] ) return input[i];
        }
    }
    return undefined;
}

//console.log(firstRecurrringCharacter([2,5,1,2,3,5,1,2,4]));
//console.log(firstRecurrringCharacter([2,1,1,2,3]));

function firstRecurrringCharacter2(input){
    let hashTable = {};
    for(let i=0; i<input.length;i++){
        if (hashTable[input[i]] !== undefined ) {
            return input[i];
        }
        else {
            hashTable[input[i]] = i;
        }
        console.log(hashTable);
    }
    return undefined;
}

//console.log(firstRecurrringCharacter2([2,5,1,2,3,5,1,2,4]));
//console.log(firstRecurrringCharacter2([2,1,1,2,3]));


let recurring = (arr) => {
    const keySet = new Map();
    for(let i = 0; i < arr.length; i++){
        if (keySet.has(arr[i])){
            return arr[i];
        }
        keySet.set(arr[i],i);
    }
    return undefined;
}

console.log(recurring([2,5,1,2,3,5,1,2,4]));
console.log(recurring([2,1,1,2,3]));
